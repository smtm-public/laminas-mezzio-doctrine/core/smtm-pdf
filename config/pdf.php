<?php

declare(strict_types=1);

namespace Smtm\Pdf;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-pdf')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-pdf'
    );
    $dotenv->load();
}

$remoteWkHtmlToPdfServices = json_decode(
    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_PDF_REMOTE_WKHTMLTOPDF_SERVICES'),
    true,
    flags: JSON_THROW_ON_ERROR
);
$fontsDir = EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_PDF_FONTS_DIR');

return [
    'pdfStuff' => json_decode(
        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_PDF_STUFF'),
        true
    ),
    'remoteWkHtmlToPdfServices' => $remoteWkHtmlToPdfServices,
    'fontsDir' => $fontsDir,
];
