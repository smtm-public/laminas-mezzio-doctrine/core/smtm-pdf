<?php

declare(strict_types=1);

namespace Smtm\Pdf;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Pdf\Infrastructure\Service\DompdfService;
use Smtm\Pdf\Infrastructure\Service\Factory\DompdfServiceFactory;
use Smtm\Pdf\Infrastructure\Service\Factory\MpdfServiceFactory;
use Smtm\Pdf\Infrastructure\Service\Factory\PdfServiceFactory;
use Smtm\Pdf\Infrastructure\Service\Factory\RemoteWkHtmlToPdfServiceFactory;
use Smtm\Pdf\Infrastructure\Service\Factory\WkHtmlToPdfServiceFactory;
use Smtm\Pdf\Infrastructure\Service\MpdfService;
use Smtm\Pdf\Infrastructure\Service\PdfLegacyService;
use Smtm\Pdf\Infrastructure\Service\PdfService;
use Smtm\Pdf\Infrastructure\Service\RemoteWkHtmlToPdfService;
use Smtm\Pdf\Infrastructure\Service\WkHtmlToPdfService;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                foreach ($container->get('config')['pdf']['remoteWkHtmlToPdfServices'] ?? [] as $serviceName => $serviceConfig) {
                    $infrastructureServicePluginManager->setFactory(
                        $serviceName,
                        function (
                            ContainerInterface $container,
                            $requestedName,
                            ?array $options = null
                        ) use ($infrastructureServicePluginManager, $serviceConfig) {
                            if ($options !== null) {
                                $serviceConfig = array_replace_recursive(
                                    $serviceConfig,
                                    $options
                                );
                            }

                            return $infrastructureServicePluginManager->build(
                                RemoteWkHtmlToPdfService::class,
                                $serviceConfig
                            );
                        }
                    );
                }

                return $infrastructureServicePluginManager->configure(
                    [
                        'factories' => [
                            PdfLegacyService::class => PdfServiceFactory::class,
                            PdfService::class => PdfServiceFactory::class,
                            MpdfService::class => MpdfServiceFactory::class,
                            DompdfService::class => DompdfServiceFactory::class,
                            WkHtmlToPdfService::class => WkHtmlToPdfServiceFactory::class,
                            RemoteWkHtmlToPdfService::class => RemoteWkHtmlToPdfServiceFactory::class,
                        ],
                    ]
                );
            }
        ],
    ],
];
