<?php

declare(strict_types=1);

namespace Smtm\Pdf;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'pdf' => 'array'
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'pdf' => include __DIR__ . '/../config/pdf.php',
        ];
    }
}
