<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Dompdf\Dompdf;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DompdfService extends AbstractInfrastructureService
{
    protected Dompdf $dompdf;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);

        $this->reset();
    }

    public function getWrappedObject(): Dompdf
    {
        return $this->dompdf;
    }

    public function reset(): static
    {
        $this->dompdf = new Dompdf();

        return $this;
    }
}
