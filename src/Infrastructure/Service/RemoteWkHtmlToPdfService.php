<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Smtm\Base\ConfigCollectionAwareInterface;
use Smtm\Base\ConfigCollectionAwareTrait;
use Smtm\Base\Infrastructure\Service\AbstractRemoteServiceConnector;
use Smtm\Pdf\Infrastructure\Service\Exception\RemoteWkHtmlToPdfException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteWkHtmlToPdfService extends AbstractRemoteServiceConnector implements ConfigCollectionAwareInterface
{

    use ConfigCollectionAwareTrait;

    public function output(
        string $body,
        array $queryParams = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): string {
        $url = $this->getConfigByName('remoteService')['baseUrl'] . ($queryParams ? '?' . http_build_query($queryParams) : '');
        $request = $this->createRequest('post', $url);
        $request = $this->setHtmlBodyOnRequest($request, $body);

        try {
            $response = $this->sendRequest($request, array_merge($options, [self::OPTION_NAME_THROW_ON_ERROR => true]));
        } catch (\Throwable $t) {
            $previous = $t->getPrevious();
            $message = $previous->getMessage();

            if ($previous instanceof ServerException || $previous instanceof ClientException) {
                $responseContents = $previous->getResponse()->getBody()->getContents();

                if (!empty($responseContents)) {
                    $message = sprintf(
                        "Remote WkHtmlToPdf request failed. Details about the error: HTTP status code '%s' and message '%s'",
                        $previous->getResponse()->getStatusCode(),
                        $responseContents
                    );
                }
            }

            throw new RemoteWkHtmlToPdfException($message, 0, $t);
        }

        return $response->getBody()->getContents();
    }

    public function help(
        array $queryParams = null,
        array $options = [
            self::OPTION_NAME_LOG_EXTRA_DATA => self::LOG_EXTRA_DATA,
        ]
    ): string {
        $queryParams['help'] = 'extended';

        $url = $this->getConfigByName('remoteService')['baseUrl'] . '?' . http_build_query($queryParams);
        $request = $this->createRequest('get', $url);

        try {
            $response = $this->sendRequest($request, array_merge($options, [self::OPTION_NAME_THROW_ON_ERROR => true]));
        } catch (\Throwable $t) {
            $previous = $t->getPrevious();
            $message = $previous->getMessage();

            if ($previous instanceof ServerException || $previous instanceof ClientException) {
                $responseContents = $previous->getResponse()->getBody()->getContents();

                if (!empty($responseContents)) {
                    $message = sprintf(
                        "Remote WkHtmlToPdf request failed. Details about the error: HTTP status code '%s' and message '%s'",
                        $previous->getResponse()->getStatusCode(),
                        $responseContents
                    );
                }
            }

            throw new RemoteWkHtmlToPdfException($message, 0, $t);
        }

        return $response->getBody()->getContents();
    }
}
