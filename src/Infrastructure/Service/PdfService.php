<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Com\Tecnick\Pdf\Tcpdf;
use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * @author Angel Baev <angel@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PdfService extends AbstractInfrastructureService
{
    protected Tcpdf $pdf;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);

        $this->reset();
    }

    public function getWrappedObject(): Tcpdf
    {
        return $this->pdf;
    }

    public function reset(): static
    {
        $this->pdf = new TCPDF('mm', true, false, true, '');

        return $this;
    }
}
