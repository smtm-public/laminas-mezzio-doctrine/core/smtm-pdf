<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Mpdf\Mpdf;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class MpdfService extends AbstractInfrastructureService
{
    protected Mpdf $mpdf;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);

        $this->reset();
    }

    public function getWrappedObject(): Mpdf
    {
        return $this->mpdf;
    }

    public function reset(): static
    {
        $this->mpdf = new Mpdf();

        return $this;
    }
}
