<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait RemoteWkHtmlToPdfServiceAwareTrait
{
    protected RemoteWkHtmlToPdfService $remoteWkHtmlToPdfService;

    public function getRemoteWkHtmlToPdfService(): RemoteWkHtmlToPdfService
    {
        return $this->remoteWkHtmlToPdfService;
    }

    public function setRemoteWkHtmlToPdfService(RemoteWkHtmlToPdfService $remoteWkHtmlToPdfService): static
    {
        $this->remoteWkHtmlToPdfService = $remoteWkHtmlToPdfService;

        return $this;
    }
}
