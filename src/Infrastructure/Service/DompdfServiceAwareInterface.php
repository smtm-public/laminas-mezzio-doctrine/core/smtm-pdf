<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface DompdfServiceAwareInterface
{
    public function getDompdfService(): DompdfService;
    public function setDompdfService(DompdfService $dompdfService): static;
}
