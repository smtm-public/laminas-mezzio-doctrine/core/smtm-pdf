<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PdfServiceAwareInterface
{
    public function getPdfService(): PdfService;
    public function setPdfService(PdfService $pdfService): static;
}
