<?php

namespace Smtm\Pdf\Infrastructure\Service\Exception;

use Smtm\Base\Infrastructure\Exception\RuntimeException;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteWkHtmlToPdfException extends RuntimeException
{
    public function __construct(string $message = '', int $code = 0, \Exception $previous = null)
    {
        if (!$message) {
            $message = 'Remote WkHtmlToPdf request failed.';
        }

        parent::__construct($message, $code, $previous);
    }
}
