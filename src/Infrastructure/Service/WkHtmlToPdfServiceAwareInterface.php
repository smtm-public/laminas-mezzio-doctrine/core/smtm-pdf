<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface WkHtmlToPdfServiceAwareInterface
{
    public function getWkHtmlToPdfService(): WkHtmlToPdfService;
    public function setWkHtmlToPdfService(WkHtmlToPdfService $wkHtmlToPdfService): static;
}
