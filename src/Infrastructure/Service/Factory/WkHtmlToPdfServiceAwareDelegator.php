<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service\Factory;

use Smtm\Base\Factory\ServiceNameAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Pdf\Infrastructure\Service\WkHtmlToPdfService;
use Smtm\Pdf\Infrastructure\Service\WkHtmlToPdfServiceAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WkHtmlToPdfServiceAwareDelegator implements DelegatorFactoryInterface, ServiceNameAwareInterface
{

    use ServiceNameAwareTrait;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var WkHtmlToPdfServiceAwareInterface $object */
        $object = $callback();

        $object->setWkHtmlToPdfService(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get($options['name'] ?? $this->getServiceName() ?? WkHtmlToPdfService::class)
        );

        return $object;
    }
}
