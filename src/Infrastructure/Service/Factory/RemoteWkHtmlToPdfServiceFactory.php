<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\Factory\RemoteServiceConnectorFactory;
use Smtm\Pdf\Infrastructure\Service\RemoteWkHtmlToPdfService;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class RemoteWkHtmlToPdfServiceFactory extends RemoteServiceConnectorFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $configPdf = $container->get('config')['pdf'];
        $configRemoteService = $options;


//        if (!defined('K_PATH_FONTS')) {
//            define('K_PATH_FONTS', $config['fontsDir']);
//        }

        /** @var RemoteWkHtmlToPdfService $service */
        $service = parent::__invoke(
            $container,
            $requestedName,
            $options
        );

        $service->addConfig('pdf', $configPdf);
        $service->addConfig('remoteService', $configRemoteService);

        return $service;
    }
}
