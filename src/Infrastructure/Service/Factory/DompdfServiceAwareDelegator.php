<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service\Factory;

use Smtm\Base\Factory\ServiceNameAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Pdf\Infrastructure\Service\DompdfService;
use Smtm\Pdf\Infrastructure\Service\DompdfServiceAwareInterface;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class DompdfServiceAwareDelegator implements DelegatorFactoryInterface, ServiceNameAwareInterface
{

    use ServiceNameAwareTrait;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var DompdfServiceAwareInterface $object */
        $object = $callback();

        $object->setDompdfService(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get($options['name'] ?? $this->getServiceName() ?? DompdfService::class)
        );

        return $object;
    }
}
