<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WkHtmlToPdfServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config')['pdf'];

//        if (!defined('K_PATH_FONTS')) {
//            define('K_PATH_FONTS', $config['fontsDir']);
//        }

        return new $requestedName(
            $container->get(InfrastructureServicePluginManager::class),
            $config
        );
    }
}
