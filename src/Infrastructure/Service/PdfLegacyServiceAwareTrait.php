<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait PdfLegacyServiceAwareTrait
{
    protected PdfLegacyService $pdfLegacyService;

    public function getPdfLegacyService(): PdfLegacyService
    {
        return $this->pdfLegacyService;
    }

    public function setPdfLegacyService(PdfLegacyService $pdfLegacyService): static
    {
        $this->pdfLegacyService = $pdfLegacyService;

        return $this;
    }
}
