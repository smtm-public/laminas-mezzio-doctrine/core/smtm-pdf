<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * @author Angel Baev <angel@smtm.bg>
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class PdfLegacyService extends AbstractInfrastructureService
{
    protected \TCPDF $pdf;

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);

        $this->reset();
    }

    public function getTcPdf(): Tcpdf
    {
        return $this->pdf;
    }

    public function setPageUnit(string $unit): static
    {
        $this->pdf->setPageUnit($unit);

        return $this;
    }

    public function setPageOrientation( string $orientation, string|bool $autopagebreak = '', string|float $bottommargin = ''): static
    {
        $this->pdf->setPageOrientation($orientation, $autopagebreak, $bottommargin);

        return $this;
    }

    public function setSpacesRE(string $re='/[^\S\xa0]/'): static
    {
        $this->pdf->setSpacesRE($re);

        return $this;
    }

    public function setRTL(bool $enable = false, bool $resetx = true): static
    {
        $this->pdf->setRTL($enable, $resetx);

        return $this;
    }

    public function getRTL(): bool
    {
        return $this->pdf->getRTL();
    }

    public function setTempRTL(string|bool $mode): static
    {
        $this->pdf->setTempRTL($mode);

        return $this;
    }

    public function isRTLTextDir(): bool
    {
        return $this->pdf->isRTLTextDir();
    }

    public function setLastH(float $h): static
    {
        $this->pdf->setLastH($h);

        return $this;
    }

    public function getCellHeight(int $fontsize, bool $padding = true): float
    {
        return $this->pdf->getCellHeight($fontsize, $padding);
    }

    public function resetLastH(): static
    {
        $this->resetLastH();

        return $this;
    }

    public function getLastH(): float
    {
        return $this->pdf->getLastH();
    }

    public function setImageScale(float $scale): static
    {
        $this->pdf->setImageScale($scale);

        return $this;
    }

    public function getImageScale(): float
    {
        return $this->pdf->getImageScale();
    }

    public function getPageDimensions(int $pagenum = 0): array
    {
        return $this->pdf->getPageDimensions($pagenum);
    }

    public function getPageWidth(int $pagenum = 0): int
    {
        return $this->pdf->getPageWidth($pagenum);
    }

    public function getPageHeight(int $pagenum = 0): int
    {
        return $this->pdf->getPageHeight($pagenum);
    }

    public function getBreakMargin(int $pagenum = 0): int
    {
        return $this->pdf->getBreakMargin($pagenum);
    }

    public function getScaleFactor(): int
    {
        return $this->pdf->getScaleFactor();
    }

    public function setMargins(int|float $left, int|float $top, int|float $right = -1, bool $keepmargins = false): static
    {
        $this->pdf->SetMargins($left, $top, $right, $keepmargins);

        return $this;
    }

    public function setLeftMargin(int|float $margin): static
    {
        $this->pdf->SetLeftMargin($margin);

        return $this;
    }

    public function setTopMargin(int|float $margin): static
    {
        $this->pdf->SetTopMargin($margin);

        return $this;
    }

    public function setRightMargin(int|float $margin): static
    {
        $this->pdf->SetRightMargin($margin);

        return $this;
    }

    public function setCellPadding(int|float $pad): static
    {
        $this->pdf->SetCellPadding($pad);

        return $this;
    }

    public function setCellPaddings(
        int|float $left = 0,
        int|float $top = 0,
        int|float $right = 0,
        int|float $bottom = 0
    ): static {
        $this->pdf->setCellPaddings($left, $top, $right, $bottom);

        return $this;
    }

    public function getCellPaddings(): array
    {
        return $this->pdf->getCellPaddings();
    }

    public function setCellMargins(int|float $left = 0, int|float $top = 0, int|float $right = 0, int|float $bottom = 0): static
    {
        $this->pdf->setCellMargins($left, $top, $right, $bottom);

        return $this;
    }

    public function getCellMargins(): array
    {
        return $this->pdf->getCellPaddings();
    }

    public function setAutoPageBreak(bool $auto = false, int|float $margin = 0): static
    {
        $this->pdf->SetAutoPageBreak($auto,$margin);

        return $this;
    }

    public function getAutoPageBreak(): bool
    {
        return $this->pdf->getAutoPageBreak();
    }

    public function setDisplayMode(
        string|int|float $zoom,
        string $layout = 'SinglePage',
        string $mode = 'UseNone'
    ): static {
        $this->pdf->SetDisplayMode($zoom, $layout, $mode);

        return $this;
    }

    public function setCompression(bool $compress = true): static
    {
        $this->pdf->SetCompression($compress);

        return $this;
    }

    public function setSRGBmode(bool $mode = false): static
    {
        $this->pdf->setSRGBmode($mode);

        return $this;
    }

    public function setDocInfoUnicode(bool $unicode = true): static
    {
        $this->pdf->SetDocInfoUnicode($unicode);

        return $this;
    }

    public function setTitle(string $title): static
    {
        $this->pdf->SetTitle($title);

        return $this;
    }

    public function setSubject(string $subject): static
    {
        $this->pdf->SetSubject($subject);

        return $this;
    }

    public function setAuthor(string $author): static
    {
        $this->pdf->SetAuthor($author);

        return $this;
    }

    public function setKeywords(string $keywords): static
    {
        $this->pdf->SetKeywords($keywords);

        return $this;
    }

    public function setCreator(string $creator): static
    {
        $this->pdf->SetCreator($creator);

        return $this;
    }

    public function setAllowLocalFiles(bool $allowLocalFiles): static
    {
        $this->pdf->SetAllowLocalFiles($allowLocalFiles);

        return $this;
    }


    public function error(string $msg): static
    {
        $this->pdf->Error($msg);

        return $this;
    }

    public function open(): static
    {
        $this->pdf->Open();

        return $this;
    }

    public function close(): static
    {
        $this->pdf->Close();

        return $this;
    }

    public function setPage(int $pnum, bool $resetmargins = false): static
    {
        $this->pdf->setPage($pnum, $resetmargins);

        return $this;
    }

    public function lastPage(bool $resetmargins = false): static
    {
        $this->pdf->lastPage($resetmargins);

        return $this;
    }

    public function getPage(): int
    {
        return$this->pdf->getPage();
    }

    public function getNumPages(): int
    {
        return $this->pdf->getNumPages();
    }

    public function addTOCPage(string $orientation = '', string|array $format = '', bool $keepmargins = false): static
    {
        $this->pdf->addTOCPage($orientation, $format, $keepmargins);

        return $this;
    }

    public function endTOCPage(): static
    {
        $this->pdf->endTOCPage();

        return $this;
    }

    public function addPage(
        string $orientation = '',
        string|array $format = '',
        bool $keepmargins = false,
        bool $tocpage = false
    ): static {
        $this->pdf->AddPage($orientation, $format, $keepmargins, $tocpage);

        return $this;
    }

    public function endPage(bool $tocpage = false): static
    {
        $this->pdf->endPage($tocpage);

        return $this;
    }

    public function startPage(string $orientation = '', string|array $format = '', bool $tocpage = false): static
    {
        $this->pdf->startPage($orientation, $format, $tocpage);

        return $this;
    }

    public function setPageMark(): static
    {
        $this->pdf->setPageMark();

        return $this;
    }

    public function setHeaderData(
        string $ln = '',
        string|int $lw = 0,
        string $ht = '',
        string $hs = '',
        array $tc = [0,0,0],
        array $lc = [0,0,0]
    ): static {
        $this->pdf->setHeaderData($ln, $lw, $ht, $hs, $tc, $lc);

        return $this;
    }

    public function setFooterData(array $tc = [0,0,0], array $lc = [0,0,0]): static
    {
        $this->pdf->setFooterData($tc, $lc);

        return $this;
    }

    public function getHeaderData(): array
    {
        return $this->pdf->getHeaderData();
    }

    public function setHeaderMargin(int $hm = 10): static
    {
        $this->pdf->setHeaderMargin($hm);

        return $this;
    }

    public function getHeaderMargin(): float
    {
        return $this->pdf->getHeaderMargin();
    }

    public function setFooterMargin(int $fm = 10): static
    {
        $this->pdf->setFooterMargin($fm);

        return $this;
    }

    public function getFooterMargin(): float
    {
        return $this->pdf->getFooterMargin();
    }

    public function setPrintHeader(bool $val = true): static
    {
        $this->pdf->setPrintHeader($val);

        return $this;
    }

    public function setPrintFooter(bool $val = true): static
    {
        $this->pdf->setPrintFooter($val);

        return $this;
    }

    public function getImageRBX(): float
    {
        return $this->pdf->getImageRBX();
    }

    public function getImageRBY(): float
    {
        return $this->pdf->getImageRBY();
    }

    public function resetHeaderTemplate(): static
    {
        $this->pdf->resetHeaderTemplate();

        return $this;
    }

    public function setHeaderTemplateAutoreset(bool $val = true): static
    {
        $this->pdf->setHeaderTemplateAutoreset($val);

        return $this;
    }

    public function header(): static
    {
        $this->pdf->Header();

        return $this;
    }

    public function footer(): static
    {
        $this->pdf->Footer();

        return $this;
    }

    public function pageNo(): int
    {
        return $this->pdf->PageNo();
    }

    public function getAllSpotColors(): array
    {
        return $this->pdf->getAllSpotColors();
    }

    public function addSpotColor(string $name, float|int $c, float|int $m, float|int $y, float|int $k): static
    {
        $this->pdf->AddSpotColor($name, $c, $m, $y, $k);

        return $this;
    }

    public function setSpotColor(string $type, string $name, float|int $tint = 100): static
    {
        $this->pdf->setSpotColor($type, $name, $tint);

        return $this;
    }

    public function setDrawSpotColor(string $name, float|int $tint = 100): static
    {
        $this->pdf->SetDrawSpotColor($name, $tint);

        return $this;
    }

    public function setFillSpotColor($name, float|int $tint = 100): static
    {
        $this->pdf->SetFillSpotColor($name, $tint);

        return $this;
    }

    public function setTextSpotColor(string $name, float|int $tint = 100): static
    {
        $this->pdf->SetTextSpotColor($name, $tint);

        return $this;
    }

    public function setColorArray(string $type, array $color, bool $ret = false): static
    {
        $this->pdf->setColorArray($type, $color, $ret);

        return $this;
    }

    public function setDrawColorArray(array $color, bool $ret = false): static
    {
        $this->pdf->SetDrawColorArray($color, $ret);

        return $this;
    }

    public function setFillColorArray(array $color, bool $ret = false): static
    {
        $this->pdf->setFillColorArray($color, $ret);

        return $this;
    }

    public function setTextColorArray(array $color, bool $ret=false): static
    {
        $this->pdf->SetTextColorArray($color, $ret);

        return $this;
    }

    public function setColor(
        string $type,
        float|int $col1 = 0,
        float|int $col2 = -1,
        float|int $col3 = -1,
        float|int $col4 = -1,
        bool $ret = false,
        string $name = ''
    ): static {
        $this->pdf->setColor($type, $col1,$col2, $col3, $col4, $ret, $name);

        return $this;
    }

    public function setDrawColor(
        float|int $col1 = 0,
        float|int $col2 = -1,
        float|int $col3 = -1,
        float|int $col4 = -1,
        bool $ret = false,
        string $name = ''
    ): static {
        $this->pdf->SetDrawColor($col1,$col2, $col3, $col4, $ret, $name);

        return $this;
    }

    public function setFillColor(
        float|int $col1 = 0,
        float|int $col2 = -1,
        float|int $col3 = -1,
        float|int $col4 = -1,
        bool $ret = false,
        string $name = ''
    ): static {
        $this->pdf->SetFillColor($col1,$col2, $col3, $col4, $ret, $name);

        return $this;
    }

    public function setTextColor(
        float|int $col1 = 0,
        float|int $col2 = -1,
        float|int $col3 = -1,
        float|int $col4 = -1,
        bool $ret = false,
        string $name = ''
    ): static {
        $this->pdf->SetTextColor($col1,$col2, $col3, $col4, $ret, $name);

        return $this;
    }

    public function setStringWidth(
        string $s,
        string $fontname = '',
        string $fontstyle = '',
        float|int $fontsize = 0,
        bool $getarray = false
    ): static {
        $this->pdf->GetStringWidth($s, $fontname, $fontstyle, $fontsize, $getarray);

        return $this;
    }

    public function getArrStringWidth(
        array $sa,
        string $fontname = '',
        string $fontstyle = '',
        float|int $fontsize = 0,
        bool $getarray = false
    ): array|float {
        return $this->pdf->GetArrStringWidth($sa, $fontname, $fontstyle, $fontsize, $getarray);
    }

    public function getCharWidth(int $char, bool $notlast = true): float
    {
        return $this->pdf->GetCharWidth($char, $notlast);
    }

    public function getRawCharWidth(int $char): float
    {
        return $this->pdf->getRawCharWidth($char);
    }

    public function getNumChars(string $s): int
    {
        return $this->pdf->GetNumChars($s);
    }

    public function addFont(
        string $family,
        string $style = '',
        string $fontfile = '',
        string|bool $subset = 'default'
    ): static {
        $this->pdf->AddFont($family, $style, $fontfile, $subset);

        return $this;
    }

    public function setFont(
        string $family,
        string $style = '',
        float|null $size = null,
        string $fontfile = '',
        string|bool $subset='default',
        bool $out = true
    ): static {
        $this->pdf->SetFont($family, $style, $size, $fontfile, $subset, $out);

        return $this;
    }

    public function SetFontSize(float $size, bool $out = true): static
    {
        $this->pdf->SetFont($size, $out);

        return $this;
    }

    public function getFontBBox(): array
    {
        return $this->pdf->getFontBBox();
    }

    public function getAbsFontMeasure(int $s) : float
    {
        return $this->pdf->getAbsFontMeasure($s);
    }

    public function getCharBBox(int $char) : array|false
    {
        return $this->pdf->getCharBBox($char);
    }

    public function getFontDescent(string $font, string $style = '', float|int $size = 0): int
    {
        return $this->pdf->getFontDescent($font, $style, $size);
    }

    public function getFontAscent(string $font, string $style = '', float|int $size = 0): int
    {
        return $this->pdf->getFontAscent($font, $style, $size);
    }

    public function isCharDefined(string|int $char, string $font = '', string $style = ''): bool
    {
        return $this->pdf->isCharDefined($char, $font, $style);
    }

    public function replaceMissingChars(string $text, string $font = '', string $style = '', array $subs = []): static
    {
        $this->pdf->replaceMissingChars($text, $font, $style, $subs);

        return $this;
    }

    public function setDefaultMonospacedFont(string $font): static
    {
        $this->pdf->SetDefaultMonospacedFont($font);

        return $this;
    }

    public function addLink(): static
    {
        $this->pdf->AddLink();

        return $this;
    }

    public function setLink(int $link, float|int $y = 0, int $page = -1): static
    {
        $this->pdf->SetLink($link, $y, $page);

        return $this;
    }

    public function link(float|int $x, float|int $y, float|int $w, float|int$h, string|int $link, int $spaces = 0): static
    {
        $this->pdf->Link($x, $y, $w, $h, $link, $spaces);

        return $this;
    }

    public function annotation(
        float|int $x,
        float|int $y,
        float|int $w,
        float|int $h,
        string $text,
        array $opt=['Subtype' => 'Text'],
        int $spaces = 0
    ): static {
        $this->pdf->Annotation($x, $y, $w, $h, $text, $opt, $spaces);

        return $this;
    }

    public function text(
        float|int $x,
        float|int $y,
        string $txt,
        int|bool $fstroke = false,
        bool $fclip = false,
        bool $ffill = true,
        string|array|int $border = 0,
        int $ln=0,
        string $align = '',
        bool $fill = false,
        string|int $link = '',
        int $stretch = 0,
        bool $ignoreMinHeight = false,
        string $calign = 'T',
        string $valign = 'M',
        bool $rtloff = false
    ): static {
        $this->pdf->Text(
            $x,
            $y,
            $txt,
            $fstroke,
            $fclip,
            $ffill,
            $border,
            $ln,
            $align,
            $fill,
            $link,
            $stretch,
            $ignoreMinHeight,
            $calign,
            $valign,
            $rtloff
        );

        return $this;
    }

    public function acceptPageBreak(): bool
    {
        return $this->pdf->AcceptPageBreak();
    }

    public function cell(
        float|int $w,
        float|int $h = 0,
        string $txt = '',
        string|array|int $border = 0,
        int $ln = 0,
        string $align = '',
        bool $fill = false,
        string|int $link = '',
        int $stretch = 0,
        bool $ignoreMinHeight = false,
        string $calign = 'T',
        string $valign = 'M'
    ): static {
        $this->pdf->Cell(
            $w,
            $h,
            $txt,
            $border,
            $ln,
            $align,
            $fill,
            $link,
            $stretch,
            $ignoreMinHeight,
            $calign,
            $valign
        );

        return $this;
    }

    public function multiCell(
        float|int $w,
        float|int $h,
        string $txt,
        string|array|int $border = 0,
        string $align = 'J',
        bool $fill = false,
        int $ln = 1,
        float|int $x = 0,
        float|int $y = 0,
        bool $reseth = true,
        int $stretch = 0,
        bool $ishtml = false,
        bool $autopadding = true,
        float|int $maxh = 0,
        string $valign = 'T',
        bool $fitcell = false
    ): int {
        return $this->pdf->MultiCell(
            $w,
            $h,
            $txt,
            $border,
            $align,
            $fill,
            $ln,
            $x,
            $y,
            $reseth,
            $stretch,
            $ishtml,
            $autopadding,
            $maxh,
            $valign,
            $fitcell
        );
    }

    public function getNumLines(
        string $txt,
        float|int $w = 0,
        bool $reseth = false,
        bool $autopadding = true,
        float|int $cellpadding = 0,
        string|array|int $border = 0
    ): float {
        return $this->pdf->getNumLines($txt, $w, $reseth, $autopadding, $cellpadding, $border);
    }

    public function getStringHeight(
        float|int $w,
        string $txt,
        bool $reseth = false,
        bool $autopadding = true,
        float|int $cellpadding = 0,
        string|array|int $border = 0
    ): float {
        return $this->pdf->getStringHeight($w, $txt, $reseth, $autopadding, $cellpadding, $border);
    }

    public function write(
        float|int $h,
        string $txt,
        string|int $link = '',
        bool $fill = false,
        string $align = '',
        bool $ln = false,
        int $stretch = 0,
        bool $firstline = false,
        bool $firstblock = false,
        float|int $maxh = 0,
        float|int $wadj = 0,
        array|string $margin = ''
    ): int|string {
        return $this->pdf->Write(
            $h,
            $txt,
            $link,
            $fill,
            $align,
            $ln,
            $stretch,
            $firstline,
            $firstblock,
            $maxh,
            $wadj,
            $margin
        );
    }

    public function setHeaderFont(array $font): static
    {
        $this->pdf->setHeaderFont($font);

        return $this;
    }

    public function getHeaderFont(): array
    {
        return $this->pdf->getHeaderFont();
    }

    public function setFooterFont(array $font): static
    {
        $this->pdf->setFooterFont($font);

        return $this;
    }

    public function getFooterFont(): array
    {
        return $this->pdf->getFooterFont();
    }

    public function setFontSubsetting(bool $enable = true): static
    {
        $this->pdf->setFontSubsetting($enable);

        return $this;
    }

    public function getFontSubsetting(): bool
    {
        return $this->pdf->getFontSubsetting();
    }

    public function setTextShadow(
        array $params = ['enabled' => false, 'depth_w' => 0, 'depth_h' => 0, 'color' => false, 'opacity' => 1, 'blend_mode' => 'Normal']
    ): static {
        $this->pdf->setTextShadow($params);

        return $this;
    }

    public function getTextShadow()
    {
        return $this->pdf->getTextShadow();
    }

    public function writeHTMLCell(
        float|int|string $w,
        float|int|string $h,
        float|int|string $x,
        float|int|string $y,
        string $html = '',
        string|array|int $border = 0,
        int $ln = 0,
        bool $fill = false,
        bool $reseth = true,
        string $align = '',
        bool $autopadding = true
    ): int {
        return $this->pdf->MultiCell(
            $w,
            $h,
            $html,
            $border,
            $align,
            $fill,
            $ln,
            $x,
            $y,
            $reseth,
            0,
            true,
            $autopadding,
            0,
            'T',
            false
        );
    }

    public function output(string $name = 'doc.pdf', string $dest = 'I'): string
    {
        return $this->pdf->Output($name, $dest);
    }

    public function reset(): static
    {
        $this->pdf = new \TCPDF(
            PDF_PAGE_ORIENTATION,
            PDF_UNIT,
            PDF_PAGE_FORMAT,
            true,
            'UTF-8',
            false
        );

        return $this;
    }
}
