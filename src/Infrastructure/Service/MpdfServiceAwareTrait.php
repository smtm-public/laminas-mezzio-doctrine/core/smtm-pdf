<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait MpdfServiceAwareTrait
{
    protected MpdfService $mpdfService;

    public function getMpdfService(): MpdfService
    {
        return $this->mpdfService;
    }

    public function setMpdfService(MpdfService $mpdfService): static
    {
        $this->mpdfService = $mpdfService;

        return $this;
    }
}
