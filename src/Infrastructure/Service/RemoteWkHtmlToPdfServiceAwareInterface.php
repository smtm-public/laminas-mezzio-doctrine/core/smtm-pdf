<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface RemoteWkHtmlToPdfServiceAwareInterface
{
    public function getRemoteWkHtmlToPdfService(): RemoteWkHtmlToPdfService;
    public function setRemoteWkHtmlToPdfService(RemoteWkHtmlToPdfService $remoteWkHtmlToPdfService): static;
}
