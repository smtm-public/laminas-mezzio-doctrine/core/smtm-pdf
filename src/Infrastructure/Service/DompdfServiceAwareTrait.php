<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait DompdfServiceAwareTrait
{
    protected DompdfService $dompdfService;

    public function getDompdfService(): DompdfService
    {
        return $this->dompdfService;
    }

    public function setDompdfService(DompdfService $dompdfService): static
    {
        $this->dompdfService = $dompdfService;

        return $this;
    }
}
