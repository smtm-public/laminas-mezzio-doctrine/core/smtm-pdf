<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

use Smtm\Base\Infrastructure\Exception\RuntimeException;
use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class WkHtmlToPdfService extends AbstractInfrastructureService
{
    protected mixed $proc = null;
    protected array $pipes = [];

    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);

        $this->reset();
    }

    public function __destruct()
    {
        $this->reset();
    }

    public function reset(): static
    {
        if (isset($this->pipes[0]) && is_resource($this->pipes[0]) && get_resource_type($this->pipes[0]) === 'stream') {
            fclose($this->pipes[0]);
        }

        if (isset($this->pipes[1]) && is_resource($this->pipes[1]) && get_resource_type($this->pipes[1]) === 'stream') {
            fclose($this->pipes[1]);
        }

        if (isset($this->pipes[2]) && is_resource($this->pipes[2]) && get_resource_type($this->pipes[2]) === 'stream') {
            fclose($this->pipes[2]);
        }

        if (is_resource($this->proc) && get_resource_type($this->proc) === 'process') {
            proc_close($this->proc);
        }

        $this->pipes = [];
        $this->proc = null;

        return $this;
    }

    public function output(string $input): string
    {
        $this->reset();

        $this->proc = proc_open(
            //'wkhtmltopdf --quiet - -',
            'wkhtmltopdf - -',
            [
                0 => ['pipe', 'r'],
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w'],
            ],
            $this->pipes
        );

        $procStatusCode = null;
        $stdout = '';
        $stderr = '';

        if (is_resource($this->proc)) {
            fwrite($this->pipes[0], $input);
            fclose($this->pipes[0]);
            $dummyRead = null;
            $dummyWrite = null;
            $dummyExcept = null;
            $read = [
                $this->pipes[1],
                $this->pipes[2],
            ];

            while ($numChangedStreams = stream_select($read, $dummyWrite, $dummyExcept, 5)) {
                if (isset($read[0])) {
                    $stdoutStat = fstat($this->pipes[1]);

                    if ($stdoutStat['size']) {
                        $stdout .= fread($this->pipes[1], $stdoutStat['size']);
                    } else {
                        $stdout .= fread($this->pipes[1], 100000);
                    }
                }

                if (isset($read[1])) {
                    $stderrStat = fstat($this->pipes[2]);

                    if ($stderrStat['size']) {
                        $stderr .= fread($this->pipes[2], $stderrStat['size']);
                    } else {
                        $stderr .= fread($this->pipes[2], 100000);
                    }
                }

                if (feof($this->pipes[1])) {
                    break;
                }

                $read = [
                    $this->pipes[1],
                    $this->pipes[2],
                ];
            }

            fclose($this->pipes[2]);
            fclose($this->pipes[1]);
            $procStatusCode = proc_close($this->proc);

            if ($numChangedStreams === false) {
                throw new RuntimeException('Failed selecting IO streams during PDF generation.');
            }
        }

        return $stdout;
    }

    public function help(bool $extended = true): string
    {
        $this->reset();

        $this->proc = proc_open(
            $extended ? 'wkhtmltopdf -H' : 'wkhtmltopdf',
            [
                //0 => ['pipe', 'r'],
                1 => ['pipe', 'w'],
                2 => ['pipe', 'w'],
            ],
            $this->pipes
        );

        $procStatusCode = null;
        $stdout = '';
        $stderr = '';

        if (is_resource($this->proc)) {
            $dummyRead = null;
            $dummyWrite = null;
            $dummyExcept = null;
            $read = [
                $this->pipes[1],
                $this->pipes[2],
            ];

            while ($numChangedStreams = stream_select($read, $dummyWrite, $dummyExcept, 5)) {
                if (isset($read[0])) {
                    $stdoutStat = fstat($this->pipes[1]);

                    if ($stdoutStat['size']) {
                        $stdout .= fread($this->pipes[1], $stdoutStat['size']);
                    } else {
                        $stdout .= fread($this->pipes[1], 100000);
                    }
                }

                if (isset($read[1])) {
                    $stderrStat = fstat($this->pipes[2]);

                    if ($stderrStat['size']) {
                        $stderr .= fread($this->pipes[2], $stderrStat['size']);
                    } else {
                        $stderr .= fread($this->pipes[2], 100000);
                    }
                }

                if (feof($this->pipes[1])) {
                    break;
                }

                $read = [
                    $this->pipes[1],
                    $this->pipes[2],
                ];
            }

            fclose($this->pipes[2]);
            fclose($this->pipes[1]);
            $procStatusCode = proc_close($this->proc);

            if ($numChangedStreams === false) {
                throw new RuntimeException('Failed selecting IO streams during PDF generation.');
            }
        }

        return $stdout;
    }
}
