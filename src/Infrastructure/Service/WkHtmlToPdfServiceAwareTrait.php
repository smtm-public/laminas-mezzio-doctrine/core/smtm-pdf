<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait WkHtmlToPdfServiceAwareTrait
{
    protected WkHtmlToPdfService $wkHtmlToPdfService;

    public function getWkHtmlToPdfService(): WkHtmlToPdfService
    {
        return $this->wkHtmlToPdfService;
    }

    public function setWkHtmlToPdfService(WkHtmlToPdfService $wkHtmlToPdfService): static
    {
        $this->wkHtmlToPdfService = $wkHtmlToPdfService;

        return $this;
    }
}
