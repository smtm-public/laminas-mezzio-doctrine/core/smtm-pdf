<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface PdfLegacyServiceAwareInterface
{
    public function getPdfLegacyService(): PdfLegacyService;
    public function setPdfLegacyService(PdfLegacyService $pdfLegacyService): static;
}
