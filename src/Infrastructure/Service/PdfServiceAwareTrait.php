<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait PdfServiceAwareTrait
{
    protected PdfService $pdfService;

    public function getPdfService(): PdfService
    {
        return $this->pdfService;
    }

    public function setPdfService(PdfService $pdfService): static
    {
        $this->pdfService = $pdfService;

        return $this;
    }
}
