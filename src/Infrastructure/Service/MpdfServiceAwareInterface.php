<?php

declare(strict_types=1);

namespace Smtm\Pdf\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface MpdfServiceAwareInterface
{
    public function getMpdfService(): MpdfService;
    public function setMpdfService(MpdfService $mpdfService): static;
}
